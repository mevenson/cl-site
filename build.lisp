(in-package :cl-user)

(pushnew (make-pathname :defaults *load-truename* :name nil :type nil)
	 quicklisp:*local-project-directories* :test #'equalp)

(ql:register-local-projects)

(ql:quickload :cl-site)

(cl-site:make-site)


